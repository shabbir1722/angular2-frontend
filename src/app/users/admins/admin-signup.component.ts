import {Component} from '@angular/core';
import { Admin } from './admin';
import { routing } from '../../routing/app.routing';
import {Router,Routes,RouterModule} from '@angular/router';
import { AdminRegisterService } from '../../services/admin-register.service';
@Component({
    selector :'admin-signup',
    templateUrl:'../../templates/admin-signup.html',
    styleUrls:['../../../assets/css/signup.css'],
    providers:[AdminRegisterService]
})
export class AdminSignupComponent{
    postData:String;
    admin:Admin={name:'',username:'',password:'',pin_code:'',company_id:'',phone:''};
    constructor(private router:Router,private adminRegisterService:AdminRegisterService){
    }
   onSignup(){
       this.adminRegisterService.signupAdmin(this.admin).subscribe(
                    data => this.postData=JSON.stringify(data),error => alert(error),()=>console.log('finished')
                );
   }
}