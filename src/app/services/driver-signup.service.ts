import { Injectable } from '@angular/core';
import { Http,RequestMethod,RequestOptions,Request,Response } from "@angular/http";
import {Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import { Driver } from '../users/drivers/driver';
import { MyUploadItem }  from '../users/uploaditem';
@Injectable()
export class DriverSignupService{
     xhr: XMLHttpRequest = new XMLHttpRequest();
    constructor(private http: Http){
        console.log('hey from RegisterService');
    }
    signupDriver(driver:Driver){
         var json=JSON.stringify(driver);
         var params= 'json=' + json;
         var headers=new Headers();
         headers.append('Content-Type', 'application/x-www-form-urlencoded');
         return this.http.post('http://localhost:3000/api/v1/driver/register',params,{
             headers: headers
         })
         .map(res => res.json());
    }
    postWithFile(file:File){
    }
}