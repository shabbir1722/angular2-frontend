import {ModuleWithProviders} from '@angular/core';
import {Routes,RouterModule} from '@angular/router';
import {AboutComponent} from '../employees/about.component';
import { MapComponent } from '../maps/map.component';
import { DriverSignupComponent } from '../users/drivers/driver-signup.component';
import { DriverLoginComponent } from '../users/drivers/driver-login.component';
import { GetDriverComponent } from '../users/drivers/get-drivers.component';
import { AddDriverComponent } from '../users/drivers/add-driver.component';
import { AdminSignupComponent } from '../users/admins/admin-signup.component';
import { AdminLoginComponent } from '../users/admins/admin-login.component';
import { GetPassengersComponent } from '../users/passengers/get-passengers.component';
import { AdminDashboardComponent } from '../users/admins/admin-dashboard.component';
const appRoutes=[
    {
        path: 'about',
        component:AboutComponent
    },

    {
        path: 'map',
        component:MapComponent
    },
    {
        path: 'Driver-Signup',
        component:DriverSignupComponent
    },
    {
        path: 'Admin-Signup',
        component:AdminSignupComponent
    },
    {
        path: 'Driver-Login',
        component: DriverLoginComponent
    },
    {
        path: 'Admin-Login',
        component:AdminLoginComponent
    },
    {
        path: 'Dashboard/Get-Drivers',
        component:GetDriverComponent
    },
    {
        path: 'Dashboard',
        component:AdminDashboardComponent
    },
    {
        path: 'Dashboard/Get-Passengers',
        component:GetPassengersComponent
    },
    {
        path: 'Dashboard/Add-Driver',
        component:AddDriverComponent
    }


];
export const routing : ModuleWithProviders = RouterModule.forRoot(appRoutes);