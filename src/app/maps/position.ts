export interface DriverPosition{
    lat:number;
    lng:number;
    name?:String;
    draggable:boolean;
}