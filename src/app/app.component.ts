﻿import { Component,EventEmitter } from '@angular/core';
import { AboutComponent } from './employees/about.component';
import { MapComponent } from './maps/map.component';
import { DriverSignupComponent } from './users/drivers/driver-signup.component';
import { DriverLoginComponent } from './users/drivers/driver-login.component';
import { AdminSignupComponent } from './users/admins/admin-signup.component';
import { AdminLoginComponent } from './users/admins/admin-login.component';
import * as io from "socket.io-client";
import { routing } from './routing/app.routing';
import {Router,Routes,RouterModule} from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './templates/app.html',
  styleUrls: ['../assets/css/app.css'],
})
export class AppComponent {
  title: string = 'My first angular2-google-maps project';
  lat: number = 51.678418;
  lng: number = 7.809007;
  //title:String;
  socket:SocketIOClient.Socket;
  constructor(){
     //this.title = 'app works!';
     this.socket = io.connect("http://localhost:3000");
     this.socket.on("connection", () => console.log("this is onl a test"));
  } 
}
