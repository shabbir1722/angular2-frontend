import { UploadItem }    from 'angular2-http-file-upload';
 
export class MyUploadItem extends UploadItem {
    constructor(file: any) {
        super();
        this.url = 'http://localhost:3000/api/v1/drivers/adddriver';
        this.headers = { 'Content-Type':'application/x-www-form-urlencoded'};
        this.file = file;
    }
}