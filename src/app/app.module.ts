import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { routing }  from './routing/app.routing';
import { AboutComponent } from './employees/about.component';
import { AdminLoginService } from './services/admin-login.service';
import { AdminRegisterService } from './services/admin-register.service';
import { GetDriversService } from './services/driversdata.service';
import { PassengersService } from './services/passengers.service';
import { AdminLoginComponent } from'./users/admins/admin-login.component';
import { AdminDashboardComponent } from'./users/admins/admin-dashboard.component';
import { GetDriverComponent } from './users/drivers/get-drivers.component';
import { DriverLoginComponent } from './users/drivers/driver-login.component';
import { AdminSignupComponent } from './users/admins/admin-signup.component';
import { DriverSignupComponent } from './users/drivers/driver-signup.component';
import { GetPassengersComponent } from './users/passengers/get-passengers.component';
import { MapComponent } from './maps/map.component';
import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';
import { AddDriverComponent } from './users/drivers/add-driver.component';
import { Uploader }      from 'angular2-http-file-upload';
import { DataTablesModule } from 'angular-datatables';
import { DriversDataComponent } from './users/drivers/drivers-data.component';
@NgModule({
  declarations: [
    AppComponent,AboutComponent,AdminLoginComponent,AdminDashboardComponent,AdminSignupComponent,
    DriverLoginComponent,DriverSignupComponent,MapComponent,GetDriverComponent,GetPassengersComponent,
    FileSelectDirective,AddDriverComponent,DriversDataComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    routing,
    DataTablesModule,
    HttpModule,AgmCoreModule.forRoot({
      apiKey: 'AIzaSyADVlCQIdTSeD3EBP_MerueKJ_I0XNkA7U'
    })
  ],
  providers: [AdminLoginService,AdminRegisterService,GetDriversService,PassengersService,Uploader],
  bootstrap: [AppComponent]
})
export class AppModule { }
