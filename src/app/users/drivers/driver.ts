export interface Driver{
    name:String;
    phone:String;
    status:boolean;
    pincode:String;
    current_lat:number;
    current_lng:number;
    cnic:String;
}