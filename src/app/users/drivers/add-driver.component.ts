import { Component,ElementRef } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { DriverSignupService } from '../../services/driver-signup.service'
import { Uploader }      from 'angular2-http-file-upload';
import { MyUploadItem }  from '../uploaditem';
import { Model } from '../../users/drivers/model';
@Component({
    selector :'get-drivers',
    templateUrl:'../../templates/add-driver.html',
    styleUrls:['../../../assets/css/style.css'],
    providers:[DriverSignupService]
})
export class AddDriverComponent {
    model:Model={username:'',email:'',password:'',age:0,phone:'',interest:'',vehicle:'',cnic:''};
    file:File;
    postData:string;
   // public uploader:FileUploader = new FileUploader({url:'http://localhost:3001/upload'});
  /*onChange(event) {
    let file = event.srcElement.files;
    console.log(file);
}*/

constructor(private element: ElementRef,private driverSignUpService:DriverSignupService,public uploaderService: Uploader) {}

    changeListner(event) {
        var reader = new FileReader();
        var image = this.element.nativeElement.querySelector('.image');
        reader.onload = function(e) {
            var src = reader.result;
            image.src = src;
        };
        this.file=event.target.files[0];
        reader.readAsDataURL(event.target.files[0]);
    }

    addDriver() {
        console.log("hello");
        console.log(this.model.username);
        console.log(this.model.age);
       // this.driverSignUpService.postWithFile(this.file);
    }
} 