import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import {Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import { Admin } from '../users/admins/admin';
@Injectable()
export class AdminRegisterService{
    constructor(private http: Http){
        console.log('hey from RegisterService');
    }
    getCurrentTime(){
         return this.http.get('http://date.jsontest.com')
         .map(res => res.json());
    }
    signupAdmin(admin:Admin){
         var json=JSON.stringify(admin);
         var params= 'json=' + json;
         var headers=new Headers();
         headers.append('Content-Type', 'application/x-www-form-urlencoded');
         return this.http.post('http://localhost:3000/api/v1/admin/register',params,{
             headers: headers
         })
         .map(res => res.json());
    }
}