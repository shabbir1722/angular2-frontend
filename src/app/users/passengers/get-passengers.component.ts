import {Component} from '@angular/core';
import { PassengersService } from '../../services/passengers.service';
import { OnInit } from '@angular/core';
declare var google: any;
@Component({
    selector :'get-drivers',
    templateUrl:'../../templates/maps.html',
    styleUrls:['../../../assets/css/maps.css'],
    providers:[PassengersService]
})
export class GetPassengersComponent implements OnInit{
    title: string = 'My first angular2-google-maps project';
    lat: number = 33.2356;
    lng: number = 73.2653;
    zoom:number= 5;
    getData:string;
    position:Position;
    latitude:number;
    self=this;
    constructor(private passengersservice:PassengersService){}
    ngOnInit() {
                this.passengersservice.getPassengersData().subscribe(
                    data => {
                        //console.log(data.length)
                        this.show(data);
                    },error => alert(error),this.finish
                );
    }
    show(data){
        if(data.success){
                
        }
        else{
            console.log(data);
        }
        console.log('finished');
    }
    finish(){}
}
