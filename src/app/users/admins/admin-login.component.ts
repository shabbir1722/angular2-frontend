import {Component} from '@angular/core';
import { Admin } from './admin';
import { routing } from '../../routing/app.routing';
import {Router,Routes,RouterModule} from '@angular/router';
import { AdminLoginService } from '../../services/admin-login.service';
@Component({
    selector :'user-component',
    templateUrl:'../../templates/admin-login.html',
    styleUrls:['../../../assets/css/login.css'],
    providers:[AdminLoginService]
})
export class AdminLoginComponent{
    postData:String;
    admin:Admin={name:'',username:'',password:'',pin_code:'',company_id:'',phone:''};
    constructor(private router:Router,private adminLoginService:AdminLoginService){}
     onLogin(){
       this.adminLoginService.loginAdmin(this.admin).subscribe(
                    data =>{
                        this.testLogin(data);
                    },error => alert(error),()=>console.log('finished')
                );
   }
 testLogin(data:any){
         if(data.success){
            window.localStorage.setItem('token',data.token);
            console.log(data.token);
            //this.router.navigate(['Get-Drivers']);
            this.router.navigate(['Dashboard']);
        }
        else{
            console.log(data);
            }
 }

}