export interface Admin {
    name:String;
    username:String;
    password:String;
    pin_code:String;
    phone:String;
    company_id:String;
}