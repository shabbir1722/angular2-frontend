import {Component} from '@angular/core';
import { OnInit } from '@angular/core';
declare var google: any;
@Component({
    selector :'get-drivers',
    templateUrl:'../templates/maps.html',
    styleUrls:['../../assets/css/maps.css'],
})
export class MapComponent{
    title: string = 'My first angular2-google-maps project';
    lat: number = 51.678418;
    lng: number = 7.809007;
}