import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import {Headers} from '@angular/http';
@Injectable()
export class PassengersService{
    token:string;
    constructor(private http: Http){
        console.log('hey from GetPassengersService');
    }
    getPassengersData(){
         this.token=window.localStorage.getItem('token');
         var headers=new Headers();
         headers.append('Content-Type', 'application/x-www-form-urlencoded');
         headers.append('x-access-token',this.token);
         return this.http.get('http://localhost:3000/api/v1/passengers/all',{
             headers: headers
         })
         .map(res => res.json());
    }
}