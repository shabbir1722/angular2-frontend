import { Angular2Proj3Page } from './app.po';

describe('angular2-proj3 App', () => {
  let page: Angular2Proj3Page;

  beforeEach(() => {
    page = new Angular2Proj3Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
