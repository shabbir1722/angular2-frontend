import { Injectable } from '@angular/core';
import {Http } from "@angular/http";
import {Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import { Admin } from '../users/admins/admin';
@Injectable()
export class AdminLoginService{
    constructor(private http: Http){
        console.log('hey from AdminLoginService');
    }
    loginAdmin(admin:Admin){
         var json=JSON.stringify(admin);
         var params= 'json=' + json;
         var headers=new Headers();
         headers.append('Content-Type', 'application/x-www-form-urlencoded');
         return this.http.post('http://localhost:3000/api/v1/admin/login',params,{
             headers: headers
         })
         .map(res => res.json());
    }
}