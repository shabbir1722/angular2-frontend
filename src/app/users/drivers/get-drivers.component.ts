import {Component} from '@angular/core';
import { GetDriversService } from '../../services/driversdata.service';
import { OnInit } from '@angular/core';
import { DriverPosition } from '../../maps/position';
declare var google: any;
@Component({
    selector :'get-drivers',
    templateUrl:'../../templates/maps.html',
    styleUrls:['../../../assets/css/maps.css'],
    providers:[GetDriversService]
})
export class GetDriverComponent implements OnInit{
    driver_position:DriverPosition={name:'',lat:0,lng:0,draggable:false};
    driver_positions:DriverPosition[]=[];
    title: string = 'My first angular2-google-maps project';
    lat: number = 33.2356;
    lng: number = 73.2653;
    zoom:number= 5;
    getData:string;
    position:Position;
    latitude:number;
    self=this;
    constructor(private getdriversservice:GetDriversService){}
    ngOnInit() {
                this.getdriversservice.getDriversData().subscribe(
                    data => {
                        //console.log(data.length)
                        this.show(data,this.driver_position);
                    },error => alert(error),this.finish
                );
    }
    show(data,driver_position){
        if(data.success){
                for (let driversdata of data.data) {
            if(driversdata.current_lat&&driversdata.current_lng){
               let driver_position:DriverPosition;
               driver_position={name:'',lat:33.2356,lng:73.2653,draggable:false};
               driver_position.lat=driversdata.current_lat;
               driver_position.lng=driversdata.current_lng;
               driver_position.name=driversdata.name;
               driver_position.draggable=true;
               this.driver_positions.push(driver_position);
            }
             else{
                console.log("no data found");
             }
              }
        }
        else{
            console.log(data);
        }
        console.log('finished');
    }
    finish(){}
}
