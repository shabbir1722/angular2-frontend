export interface Model{
    username:String;
    email:String;
    password:String;
    age:number;
    phone:String;
    cnic:String;
    vehicle:String;
    interest:String;
}