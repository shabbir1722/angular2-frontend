import {Component} from '@angular/core';
import { Driver } from './driver';
import { routing } from '../../routing/app.routing';
import {Router,Routes,RouterModule} from '@angular/router';
import { DriverSignupService } from '../../services/driver-signup.service';
import { OnInit } from '@angular/core';
@Component({
    selector :'driver-signup',
    templateUrl:'../../templates/driver-signup.html',
    styleUrls:['../../../assets/css/signup.css'],
    providers:[DriverSignupService]
})
export class DriverSignupComponent implements OnInit{
    postData:string;
    latitude:number;
    longitude:number;
    private driver:Driver={name:'',phone:'',pincode:'',status:false,current_lat:0,current_lng:0,cnic:''};
    constructor(private router:Router,private driverSignupService:DriverSignupService){
    }
     ngOnInit() {
          if (navigator.geolocation) {
            var self = this;
            navigator.geolocation.getCurrentPosition(function(response){
                self.setPosition(response, self);
            }, function() {
            alert("Unable to get GPS Location");
            }, {
            enableHighAccuracy : true
            });
        }
        else {
        alert("Geolocation is not supported by this browser.");
        }
    }
    onSignup(driver){
       this.driverSignupService.signupDriver(this.driver).subscribe(
                    data => this.postData=JSON.stringify(data),error => alert(error),()=>console.log('finished')
                );
    }
    setPosition(position:any, self:any) {
       this.driver.current_lat=position.coords.latitude;
       this.driver.current_lng=position.coords.longitude;
       this.driver.status=true;
    }
}